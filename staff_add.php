<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Create Record</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <style type="text/css">
        .wrapper{
            width: 500px;
            margin: 0 auto;
        }
    </style>
</head>
<body>
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header">
                        <h2>Create Record</h2>
                    </div>
                    <p>Please fill this form and submit to add employee record to the database.</p>

                    <form action="staff_add_result.php" method="post">

                        <div class="form-group">
                          <label for="staff_name">Staff Name:</label>
                          <input type="text" name="staff_name" id="staff_name" class="form-control">
                        </div>

                        <div class="form-group">
                          <label for="staff_department">Staff Department:</label>
                          <input type="text" name="staff_department" id="staff_department" class="form-control">
                        </div>

                        <div class="form-group">
                          <label for="staff_role">Staff Role:</label>
                          <input type="text" name="staff_role" id="staff_role" class="form-control">
                        </div>

                        <div class="form-group" >
                          <label for="staff_team">Staff Team:</label>
                          <input type="text" name="staff_team" id="staff_team" class="form-control">
                        </div>
                        <div class="form-group">
                          <label for="staff_team">Skill Name:</label>
                      	  <select  data-rel="chosen" name="skill_id" id="skill_id" class="form-control">

                    	        <?php
                              include("staff_functions.php");
                              $staff = new Staff();
                              $skills = $staff->getAllSkills();
                    					foreach($skills as $skillName) {
                    						echo "<option value=$skillName[skill_id]>$skillName[skill_name]</option>";

                              };
                            ?>
                          </div>

                        <input type="submit" class="btn btn-primary" value="Submit">
                        <a href="staff_index.php" class="btn btn-default">Cancel</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
