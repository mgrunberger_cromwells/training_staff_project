<?php
include("staff_functions.php");
$staff = new Staff();
$skills = $staff->getAllSkillsArray();

// Check existence of staff_id parameter before processing further
if(isset($_GET["staff_id"]) && !empty(trim($_GET["staff_id"]))) {
    $result = $staff->getStaff($_GET["staff_id"]);

    if (!empty($result)) {
      foreach($result as $key=>$row)
      {
        // Retrieve individual field value
        $staff_name = $row["staff_name"];
        $staff_department = $row["staff_department"];
        $staff_role = $row["staff_role"];
        $staff_team = $row["staff_team"];
        $skill_id = $row['skill_id'];
        $skill_name = $skills[$skill_id];
      }
    } else {
        // URL doesn't contain valid staff_id parameter. Redirect to error page
        header("location: error.php");
        exit();
    }
} else {
    // URL doesn't contain staff_id parameter. Redirect to error page
    header("location: error.php");
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>View Record</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <style type="text/css">
        .wrapper{
            width: 500px;
            margin: 0 auto;
        }
    </style>
</head>
<body>
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header">
                        <h1>View Record</h1>
                    </div>
                    <div class="form-group">
                        <label>Employee Name</label>
                        <p class="form-control-static"><?php echo $staff_name; ?></p>
                    </div>
                    <div class="form-group">
                        <label>Employee Department</label>
                        <p class="form-control-static"><?php echo $staff_department; ?></p>
                    </div>
                    <div class="form-group">
                        <label>Employee Role</label>
                        <p class="form-control-static"><?php echo $staff_role; ?></p>
                    </div>
                    <div class="form-group">
                        <label>Employee Team</label>
                        <p class="form-control-static"><?php echo $staff_team; ?></p>
                    </div>
                    <div class="form-group">
                        <label>Skill Name</label>
                        <p class="form-control-static"><?php echo $skill_name; ?></p>
                    </div>
                    <p><a href="staff_index.php" class="btn btn-primary">Back</a></p>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
