<?php require_once(dirname(__FILE__).'/functions.php'); ?>
        
    <!DOCTYPE html>
    <html lang="eng">
      <head>
        <meta charset="utf-8" />
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1.0, shrink-to-fit=no"
        />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <link rel="stylesheet" href="styles/bootstrap.min.css" />
        <link rel="stylesheet" href="styles/main.css" type="text/css" />
    
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <link
          rel="stylesheet"
          href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css"
        />
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js"></script>
    
        <link
          href="https://fonts.googleapis.com/css?family=Lato:400,700&amp;subset=latin-ext"
          rel="stylesheet"
        />
        <link
          href="https://fonts.googleapis.com/css?family=Dancing+Script"
          rel="stylesheet"
        />
        <title>Header2</title>
      </head>
      <body>
        <header>
        <div class="jumbotron" style="margin-bottom:0px">
          <p>Staff Project</p>
          <h3>WEB PROJECTS</h3>
        </div>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark sticky-top">
          <button
            class="navbar-toggler"
            data-toggle="collapse"
            data-target="#collapse_target"
          >
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="collapse_target">
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link" href="#">Main Page</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="project-web.php">All projects</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="project-add-new.php">Add new project</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="project-to-customer.php">Assign a project to a customer</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="project-to-staff.php">Project to a staff member</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="project-edit.php">Edit the project</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="project-update-status.php">Update project status</a>
              </li>
            </ul>
          </div>
        </nav>
    </header>