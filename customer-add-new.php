<?php include('header.php'); ?>
<?php include('Customer.php'); ?>
<?php include('Connect_DB.php'); ?>
<main>
<div class="container">
        <div class="row">
        <div class="col-sm-6 col-md-6">

<h5>Add New Customer</h5>
<form action="customer-add-new.php" method="post">
   <table>
      <tr><td>Customer Name</td><td><input type=text name="customer_name"></td></tr>
      <tr><td>Customer Email</td><td><input type=text name="customer_email"></td></tr>
      <tr><td>Customer Phone</td><td><input type=text name="customer_phone"></td></tr>
      <tr><td><input type="submit"  name="addCustomer" style='background-color:green;color:white;' value="Add new customer"></td><td></tr>
      
   </table>
 </form>   

</div>
<div class="col-sm-6 col-md-6">                     
<?php   
      
   if($_SERVER['REQUEST_METHOD'] == "POST" and isset($_POST['addCustomer'])){
    $customer_name=$_POST['customer_name'];
    $customer_email=$_POST['customer_email'];
    $customer_phone=$_POST['customer_phone'];
    //echo $customer_name;
    $customer = new Customer();
    $customer->newCustomer($customer_name,$customer_email,$customer_phone);
    
   }
   //HTML table
   $customer2=new Customer();
   $result = $customer2->getAllCustomers();
   echo "<table border=2 spacing=2>";
   echo "<th>ID</th><th>CUSTOMER NAME</th><th>EMAIL</th><th>PHONE NO</th>";
   foreach($result as $key=>$row)
   {
   
     echo "<tr>";
     foreach($row as $cell)
     {
       echo "<td>$cell</td>";
     }
     
     echo "</tr>";
   }
   echo "</table>";   

  
?>                

          </div>
        </div>
      </div>
    </main>
  </body>
</html>
