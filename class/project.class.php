<?php
class Project{
  private $db;
	function __construct()
	{
			$this->db = new Database();
			$this->db->connect();
	}
  function getAll(){
    $do= $this->db->query("SELECT project_id, project_name, DATE_FORMAT(project_startdate, '%d/%m/%Y'),
    DATE_FORMAT(project_enddate, '%d/%m/%Y') , customer_name, color, status
    FROM project
    left join customers on project.customer_id=customers.customers_id
    LEFT join status on project.project_status=status.status_id order by project_id", "select");
    return $do;
  }
  function getOne($id){
    $do= $this->db->query("SELECT project.project_name,DATE_FORMAT(project_startdate, '%Y-%m-%d') as project_startdate,
     DATE_FORMAT(project_enddate, '%Y-%m-%d') as project_enddate , customers.customer_name, customers.customers_id, status.status,
     status.status_id, project.project_id
     FROM project
     left join customers on project.customer_id =customers.customers_id
     left join status on project.project_status =status.status_id where project_id='$id'", "select");
    return $do;
  }
  function deletequery($id) {
      $do= $this->db->query("DELETE from project where project_id='$id' ", "");
  }
  function update($id, $arr){
    $q="Update project set project_name='$arr[project_name]', project_startdate='$arr[project_startdate]',
     project_enddate='$arr[project_enddate]', customer_id='$arr[customers_id]',
     project_status='$arr[status_id]' WHERE project_id='$id'";
    $do= $this->db->query($q, "");
  }
  function insert ($arr){
    $q="insert into project values (null,'$arr[project_name]', '$arr[project_startdate]', '$arr[project_enddate]', '$arr[customers_id]', '1' )";
    $do= $this->db->query($q, "");
  }
  //select of all the customers, with the one selected
  function callCustomer($id){
    $q="select * from customers order by customer_name";
    $do_query= $this->db->query($q, "select");

    $o="<select class='form-control' name ='customers_id'>";
    foreach ($do_query as $a) {
      if($id==$a['customers_id']) $s="selected"; else  $s="";
         $o.="<option value='$a[customers_id]' $s>$a[customer_name]</option>";
    }

    $o.="</select>";
    return $o;
  }
  function callStatus($id){
    $q="select * from status order by status";
    $do_query= $this->db->query($q, "select");

    $o="<select class='form-control' name ='status_id'>";
    foreach ($do_query as $a) {
      if($id==$a['status_id']) $s="selected"; else  $s="";
         $o.="<option value='$a[status_id]' $s>$a[status]</option>";
    }

    $o.="</select>";
    return $o;
  }

  function callStaff($project){
    $q="select * from staff
where staff_id not in (select staff_id from staff_to_project where project_id='$project')
order by staff_name";
    $do_query= $this->db->query($q, "select");
    $o="<table class='table table col-sm-6'><tr><td>";
    $o.="<select class='form-control' name ='staff_id'>";
    foreach ($do_query as $a) {
         $o.="<option value='$a[staff_id]'>$a[staff_name]</option>";
    }

    $o.="</select></td><td><button class='btn btn-success'>Add</button></td></tr></table>";
    return $o;
  }

  //function for list all the skill of a staff member
  function listStaff($id){
    $q="SELECT staff_name, staff_department, staff_role, staff_team,sp_id  FROM staff_to_project
left join staff on staff_to_project.staff_id=staff.staff_id
where project_id='$id'";
    $do_query= $this->db->query($q, "select");
    return $do_query;
  }
  //funtion for add a skill to a staff member
  function addStaff($id,$project){
    $q="insert into staff_to_project values (null,'$id', $project)";
    $do_query= $this->db->query($q, "");
  }
  //skill for delete a skill to a staff member
  function removeStaff($id){
    $q= "DELETE from staff_to_project where sp_id='$id' ";
    $do=$this->db->query($q, "");
  }


}
?>
