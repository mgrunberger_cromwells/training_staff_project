<?php
  require "class/database.class.php";
  require "class/general.class.php";
  require "class/html.class.php";
  $h=new HTMLPage;
echo $h->head();
echo $h->bodystart();
echo $h->navbar();
echo $h->aside();
?>
<div class="content-wrapper">
  <section class="content">
    <div class="card">
      <div class="card-body">
<h3>Projects </h3>
        	<?php
          $general=new General();
        ?>
<div id="gantt_here" style='width:100%; height:400px;'></div>

      </div>

    </div>
  </section>
</div>



<?php
  echo $h->footer();
?>
<script>
  gantt.config.readonly = true;
  gantt.config.scale_height = 50;
  	gantt.config.scales = [
  		{unit: "month", step: 1, format: "%F, %Y"},
  		{unit: "day", step: 1, format: "%j, %D"}
  	];
	var tasks = {
		data: [
		    <?php echo $general->gantt();?>
		]
		};
    gantt.config.columns = [
    {name:"text",       label:"Task name",  width:"150" },
    {name:"start_date", label:"Start time", align:"center" }

];

    gantt.attachEvent("onTaskDblClick", function(id,e){
      window.location.href = 'updateProject.php?id='+id;

    });

	gantt.init("gantt_here");


	gantt.parse(tasks);

</script>
