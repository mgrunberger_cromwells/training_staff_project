<?php include('header2.php'); ?>
<?php include('Connect_DB.php'); ?>
<?php include('Project.class.php'); ?>

<main>
  <div class="container">
    <h5>Project Assign To Staff Member</h5>
  <div class="row">
    
    <div class="col-sm-6 col-md-6">

          
        <?php          
           //HTML table
          $project = new Project();
          $result = $project->getAllProjects();
          echo "<table border=2 spacing=2>";
          echo "<th>ID</th><th>PROJECT NAME</th><th>START DATE</th><th>END DATE</th><th>CUSTOMER ID</th><th>PROJECT STATUS</th>";
          foreach($result as $key=>$row)
          {
            
            echo "<tr>";
            foreach($row as $cell)
            {
              echo "<td>$cell</td>";
            }
            
            echo "</tr>";
          }
          echo "</table>"; 
      ?>   


    </div>

    <div class="col-sm-6 col-md-6">
                     
      <?php   
          
          $customer = new Project();
          //HTML table
          $result = $customer->allStaffMembers();
        echo "<table border=2 spacing=2>";
        echo "<th>ID</th><th>STAFF NAME</th><th>DEPARTMENT</th><th>ROLE</th><th>TEAM</th>";
        foreach($result as $key=>$row)
        {
        
          echo "<tr>";
          foreach($row as $cell)
          {
            echo "<td>$cell</td>";
          }
          
          echo "</tr>";
        }
        echo "</table>";          
      ?>            
    </div>
  </div>
  <div class="row">
    <div class="col-sm-6 col-md-6">
          <form action="customer-assign-to-project.php" method="post">
          <table>
              <tr><td>Input Customer ID</td><td><input type=text name="customer_id"></td></tr>
              <tr><td>Input Project ID</td><td><input type=text name="project_id"></td></tr>              
              <tr><td><input type="submit" name="assignProjectToCustomer" style='background-color:green;color:white;' value="Assign - Press Twice"></td><td></tr>
              
          </table>
        </form>

        <?php
            if($_SERVER['REQUEST_METHOD'] == "POST" and isset($_POST['assignProjectToCustomer']))
            {
              $customer_id=$_POST['customer_id'];
              $project_id=$_POST['project_id'];              
              //echo $customer_name;
              $customer = new Customer();
              $customer->assignCustomerToProject($customer_id,$project_id);
            }   
        ?>
        
    
  

    </div>
  </div>
 </div>

 </main>
</body>
</html>
