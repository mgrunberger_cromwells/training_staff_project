<?php
include("db_connection.php");

class Staff
{
	private $database;

	function __construct()
	{
			$this->database = new Database();
			$this->database->connect();
	}

	function getAllStaff()
	{
		$result = $this->database->send_query("select * from staff");
		//var_dump($result);
		return $result;
	}

	function getAllSkills()
	{
		$result = $this->database->send_query("select * from skills");
		return $result;
	}

	function getAllSkillsArray()
	{
		$result = $this->getAllSkills();
		foreach($result as $key=>$row) {
      $new_array[$row['skill_id']] = $row['skill_name'];
    }
	  return $new_array;
	}

	function addStaff($name, $department, $role, $team, $skill_id)
	{
	  $sql = "INSERT INTO staff (staff_name, staff_department, staff_role, staff_team, skill_id) VALUES ('$name', '$department', '$role', '$team', '$skill_id')";
		$this->database->query($sql);
	}

	function updateStaff($staff_id, $name, $department, $role, $team, $skill_id)
	{


		$sql = "UPDATE staff SET staff_name='$staff_name', staff_department='$staff_department', staff_role='$staff_role', staff_team='$staff_team', skill_id='$skill_id' WHERE staff_id='$staff_id'";
		$this->database->query($sql);
		//var_dump($sql);
	}

	function deleteStaff($staff_id)
	{
		$sql = "DELETE FROM staff WHERE staff_id='$staff_id'";
		$this->database->query($sql);
		//var_dump($sql);

	}

  //keep working
	function getStaff($staff_id)
	{
		$result = $this->database->send_query("select * from staff where staff_id='$staff_id'");
		//var_dump($result);
		return $result;
	}

}
?>
