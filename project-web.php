<?php include('header2.php'); ?>
<?php include('Customer.php'); ?>
<?php include('Connect_DB.php'); ?>
<?php include('Project.class.php'); ?>

<main>
   <div class="container">
      <h5>All Projects</h5>
      <div class="row">
        <div class="col-sm-6 col-md-6">          
            <?php          
            
            //HTML table
            $project = new Project();
            $result = $project->getAllProjects();
            echo "<table border=2 spacing=2>";
            echo "<th>ID</th><th>PROJECT NAME</th><th>START DATE</th><th>END DATE</th><th>CUSTOMER ID</th><th>PROJECT STATUS</th>";
            foreach($result as $key=>$row)
            {                
                echo "<tr>";
                foreach($row as $cell)
                {
                echo "<td>$cell</td>";
                }
                
                echo "</tr>";
            }
            echo "</table>";  
            ?>  
        </div> 
    
    </div>
   </div>
 </main>
</body>
</html>

<!-- Add a project

Assign a project to a customer

Assign a project to a staff member

Edit the project

Update project status

Delete the project -->