<html>
<?php
include("staff_functions.php");
$staff = new Staff();
$result = $staff->getAllStaff();
$skills = $staff->getAllSkillsArray();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Dashboard</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.js"></script>
    <style type="text/css">
        .wrapper{
            width: 650px;
            margin: 0 auto;
        }
        .page-header h2{
            margin-top: 0;
        }
        table tr td:last-child a{
            margin-right: 15px;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
</head>
<body>
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header clearfix">
                        <h2 class="pull-left">Staff details</h2>
                        <a href="staff_add.php" class="btn btn-success pull-right">Add new employee</a>
                    </div>
                    <?php
                    // Attempt select query execution
                        if ($result > 0) {
                            echo "<table class='table table-bordered table-striped'>";
                                echo "<thead>";
                                    echo "<tr>";
                                        echo "<th>Staff ID</th>";
                                        echo "<th>Employee Name</th>";
                                        echo "<th>Employee Department</th>";
                                        echo "<th>Employee Role</th>";
                                        echo "<th>Employee Team</th>";
                                        echo "<th>Employee Skills</th>";
                                        echo "<th>Action</th>";
                                    echo "</tr>";
                                echo "</thead>";
                                echo "<tbody>";
                                foreach($result as $key=>$row){
                                    echo "<tr>";
                                        echo "<td>" . $row['staff_id'] . "</td>";
                                        echo "<td>" . $row['staff_name'] . "</td>";
                                        echo "<td>" . $row['staff_department'] . "</td>";
                                        echo "<td>" . $row['staff_role'] . "</td>";
                                        echo "<td>" . $row['staff_team'] . "</td>";
                                        $skill_id = $row['skill_id'];
                                        $skill_name = $skills[$skill_id];
                                        echo "<td>" .$skill_name . "</td>";
                                        echo "<td>";
                                            echo "<a href='staff_read.php?staff_id=". $row['staff_id'] ."' title='View Record' data-toggle='tooltip'><span class='glyphicon glyphicon-eye-open'></span></a>";
                                            echo "<a href='staff_update.php?staff_id=". $row['staff_id'] ."' title='Update Record' data-toggle='tooltip'><span class='glyphicon glyphicon-pencil'></span></a>";
                                            echo "<a href='testing.php?staff_id=". $row['staff_id'] ."' title='Delete Record' data-toggle='tooltip'><span class='glyphicon glyphicon-trash'></span></a>";
                                        echo "</td>";
                                    echo "</tr>";
                                }
                                echo "</tbody>";
                            echo "</table>";
}
                    ?>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
