<?php include('header.php'); ?>
<?php include('Customer.php'); ?>
<?php include('Connect_DB.php'); ?>
<main>

   <div class="container">
      
      <div class="row">
      
        <div class="col-sm-12 col-md-12 offset-md-1"> 
        <h5>All Customers</h5>                    
<?php   
   //HTML table   
   $customer = new Customer();
   $result = $customer -> getAllCustomers(); 

   echo "<table border=2 spacing=2>";
   echo "<th>ID</th><th>CUSTOMER NAME</th><th>EMAIL</th><th>PHONE NO</th>";
   foreach($result as $key=>$row){
   
     echo "<tr>";
     foreach($row as $cell)
     {
       echo "<td>$cell</td>";
     }     
     echo "</tr>";
   }
   echo "</table>";    
?>                
         </div>
       </div>
   </div>
 </main>
</body>
</html>
