<?php
  require "class/database.class.php";
  require "class/staff.class.php";
    require "class/general.class.php";
  require "class/html.class.php";
  $h=new HTMLPage;
echo $h->head();
echo $h->bodystart();
echo $h->navbar();
echo $h->aside();
?>
<div class="content-wrapper">
  <section class="content">
    <div class="card">
      <div class="card-body">
        <h3 class="text-info">Update Staff</h3>
        <form action="doupStaff.php?id=<?php echo $_GET['id'];?>" method="post">
          <?php
          $staff= new Staff();
          $one=$staff->getOne($_GET['id']);
          ?>
            <input  name='staff_id' class='form-control' hidden value='<?php echo $one[0]['staff_id'];?>' type='text'>

            <label class='col-sm-4'>Staff name:<input  name='staff_name' class='form-control'  value='<?php echo $one[0]['staff_name'];?>' type='text'>
            </label>
            <label class='col-sm-4'>Staff department:<input  name='staff_department' class='form-control'  value='<?php echo $one[0]['staff_department'];?>' type='text'>
            </label>
            <label class='col-sm-4'>Staff role:<input  name='staff_role' class='form-control'  value='<?php echo $one[0]['staff_role'];?>' type='text'>
            </label>
            <label class='col-sm-4'>Staff team:<input  name='staff_team' class='form-control'  value='<?php echo $one[0]['staff_team'];?>' type='text'>
            </label>
        <hr>
        <button class="btn btn-success">Save</button> | <a href="staff.php" class="btn btn-warning">Back</a>
        <hr>
        <?php
          echo "<h3 class='text-info'>Skills</h3>";
          $list=$staff->listSkill($_GET['id']);

        ?>
        <div class="row">
        <table class="table col-sm-6">
        <thead><th>Skills</th><th>#</th>
        </thead>
        <tbody>
        <?php
           foreach($list as $key=>$row)
           {
             foreach($row as $k=> $cell)
             {
                if($k=="ss_id"){
                 $ssid=$cell;
             }
               else {
                 echo "<td>$cell</td>";
               }
             }
               echo "<td> <a href='removeSkill.php?ssid=$ssid' class='btn btn-danger'>Remove</a></td>";

               echo "</tr>";
            }
         ?>
        </tbody>
        </table>
        </form>

        <form action="addskill.php?id=<?php echo $_GET['id'];?>" class='col-sm-6' method="post">
        <?php
          $skill= $staff->callSkill();
          echo $skill;
        ?>
      </form>
       </div>
    </div>
  </section>
</div>
<?php
  echo $h->footer();
?>
