<?php include('header.php'); ?>
<?php include('Customer.php'); ?>
<?php include('Connect_DB.php'); ?>

<main>
<div class="container">
  <div class="row">
    <div class="col-sm-6 col-md-6">

        <h5>Delete The Customer</h5>  
        <?php   
          
          $customer = new Customer();
          $result = $customer -> getAllCustomers();
            
        
        //checking if variable GET is set  
          if($_SERVER['REQUEST_METHOD'] == "GET" and isset($_GET['customer_id'])){
            $id=$_GET['customer_id']; 
                $customer -> deleteCustomer($id);
          }
          $result = $customer -> getAllCustomers();
          echo "<table border=2 spacing=2>";
          echo "<th>ID</th><th></th><th>CUSTOMER NAME</th><th>EMAIL</th><th>PHONE NO</th>";     
          foreach($result as $key=>$row)
          {   
                  echo "<tr>";
                  foreach($row as $rowkey=>$cell)
                  {
                          echo "<td>$cell</td>";
                      if ($rowkey=="customer_id")
                      {
                              echo"<td><a style='background-color:red;color:black;width:40px;
                          ' href='customer-delete.php?customer_id=".$row['customer_id']."'>Delete</a></td>";                              
                      }              
                  }       
                  
              } 
          echo "</table>";     
        ?> 
      </div>  

   </div>
  </div>
    </main>
  </body>
</html>
