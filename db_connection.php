<?php

//Creating a class called Database
class Database
{
	// Private can only be accessible inside this class
	private $connection;

	function connect()
	{
		$this->connection = mysqli_connect("localhost","root","","project01");
	}

	function query($sql)
	{
		if (mysqli_query($this->connection, $sql)) {
		    echo "Records Inserted/Deleted/Updated successfully.";
		} else {
		    echo "ERROR: Could not able to execute $sql. " . mysqli_error($this->connection);
		}
	}

  // creating a function called send_query
	function send_query($sql)
	{
		$result = mysqli_query($this->connection, $sql);
		$arr = array();
		while($row = mysqli_fetch_assoc($result))
		{
			$arr[] = $row;
		}
		return $arr;
	}
}
?>
