<?php
  require "class/database.class.php";
  require "class/project.class.php";
  require "class/html.class.php";
  require "class/general.class.php";

  $h=new HTMLPage;
echo $h->head();
echo $h->bodystart();
echo $h->navbar();
echo $h->aside();
?>
<div class="content-wrapper">
  <section class="content">
    <div class="card">
      <div class="card-body">
        <h3 class="text-info">Project</h3>
        <?php
          $project= new Project();
          $all= $project->getAll();
          $general=new General();
          echo $general->newbutton('New Project','createProject.php','info' );
        ?>
        <table class="table">
        <thead>
        <th>#</th><th>Name</th><th>Start Date</th><th>End Date</th><th>Customer</th><th>Status</th><th>#</th>
        </thead>
        <tbody>
          <?php

          foreach($all as $key=>$row)
          {
            foreach($row as $k=> $cell)
            {
              switch ($k) {
                  case 'status':

                    echo "<td> $color $cell</span></td>";

                    break;
                  case "color":
                  $color="<span class='badge badge-".$cell ."'>";
                  break;
                  default:
                    echo "<td>$cell</td>";
                    break;
                }
              if($k=="project_id")
              {
                $id=$cell;
              }
            }
            echo "<td><a href='updateProject.php?id=$id' class='btn btn-warning'>Update</a>
                  <a href='deleteProject.php?id=$id' class='btn btn-danger'>Delete</a></td>";
            echo "</tr>";
           }
           ?>
         </tbody>
         </table>
       </div>
    </div>
  </section>
</div>
<?php
  echo $h->footer();
?>
