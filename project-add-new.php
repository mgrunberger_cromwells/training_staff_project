<?php include('header2.php'); ?>
<?php include('Project.class.php'); ?>
<?php include('Connect_DB.php'); ?>
<main>
<div class="container">
        <div class="row">
        <div class="col-sm-6 col-md-6">

<h5>Add New Project</h5>
<form action="project-add-new.php" method="post">
   <table>
      <tr><td>PROJECT NAME</td><td><input type=text name="project_name"></td></tr>
      <tr><td>START DATE</td><td><input type=text name="project_startdate"></td></tr>
      <tr><td>END DATE</td><td><input type=text name="project_enddate"></td></tr>
      <tr><td>PROJECT STATUS</td><td><input type=text name="project_status"></td></tr>
      <tr><td><input type="submit"  name="addProject" style='background-color:green;color:white;' value="Add new project"></td><td></tr>
      
   </table>
 </form>   

</div>
<div class="col-sm-6 col-md-6">                     
<?php   
     
   if($_SERVER['REQUEST_METHOD'] == "POST" and isset($_POST['addProject']))
   {
      
        $project_name=$_POST['project_name'];
        $project_startdate=$_POST['project_startdate'];
        $project_enddate=$_POST['project_enddate'];
        $project_status=$_POST['project_status'];
        
        $project = new Project();
        $project->newProject($project_name,$project_startdate,$project_enddate,$project_status);
    
   }
   //HTML table
   $project2=new Project();
   $result = $project2->getAllProjects();
   echo "<table border=2 spacing=2>";
   echo "<th>ID</th><th>PROJECT NAME</th><th>START DATE</th><th>END DATE</th><TH>CUSTOMER ID</TH><TH>PROJECT STATUS</TH>";
   foreach($result as $key=>$row)
   {
   
     echo "<tr>";
     foreach($row as $cell)
     {
       echo "<td>$cell</td>";
     }
     
     echo "</tr>";
   }
   echo "</table>";     
?>                

          </div>
        </div>
      </div>
    </main>
  </body>
</html>